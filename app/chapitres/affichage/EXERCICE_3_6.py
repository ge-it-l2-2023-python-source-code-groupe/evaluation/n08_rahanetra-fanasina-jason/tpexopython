#--------------------------------------------EXO 1-----------------------------------------------------------
def affichage():
    print("\nDans l'interpreteur python lorsque je tape 1+1, en appyant sur entrer l'interpreteur affiche 2"
          "\nTandis que si j'ecris la meme chose sur un script test.py dans un editeur, le resultat ne"
          " s'affiche pas."
          f"\nPour afficher le resultat on utilise la methode print donc print(1+1) qui renvoie {1+1}")

#--------------------------------------------EXO 2-----------------------------------------------------------

def poly_A():
    print("\n")
    print ("A" * 20)

#--------------------------------------------EXO 3-----------------------------------------------------------

def poly_A_G():
    print("\n")
    print ("A" * 20 + ("GC"*40))

#--------------------------------------------EXO 4-----------------------------------------------------------

def ecriture_Formate():
    print("\n")
    a="salut"
    b=102
    c = 10.318
    print("{} {} {:.2f}".format(a,b,c))

#--------------------------------------------EXO 5-----------------------------------------------------------

def ecriture_Formate2():
    perc_GC = ((4500 + 2575)/14800) * 100
    print(f"\nLe pourcentage de GC est {perc_GC:.0f} %") # ou encore print(f"Le pourcentage de GC est {perc_GC:d} %") 
    print("Le pourcentage de GC est {:.1f} %".format(perc_GC))
    print(f"Le pourcentage de GC est {perc_GC:.2f} %")
    print(f"Le pourcentage de GC est {perc_GC:.3f} %")


